<?php

namespace Drupal\commerce_minter\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentGatewayFormBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Minter\MinterAPI;
use Minter\SDK\MinterWallet;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MinterPaymentForm extends PaymentGatewayFormBase implements ContainerInjectionInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The Minter API instance.
   *
   * @var \Minter\MinterAPI
   */
  protected $minterApi;

  /**
   * {@inheritDoc}
   */
  public function __construct(RouteMatchInterface $route_match, MinterAPI $minter_api) {
    $this->routeMatch = $route_match;
    $this->minterApi = $minter_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('minter.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->routeMatch->getParameter('commerce_order');

    $wallet = $order->getData('minter_wallet');
    if (empty($wallet)) {
      $wallet = MinterWallet::create();
      $order->setData('minter_wallet', $wallet);
      $order->save();
    }

    $form['#prefix'] = '<div id="payment-form-wrapper">';
    $form['#suffix'] = '</div>';

    $form['payment_instructions'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => $this->t('Please send the total order value to the following wallet:')
    ];
    // TODO: Add copy button.
    $form['wallet'] = [
      '#type' => 'html_tag',
      '#tag' => 'strong',
      '#value' => $wallet['address'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $this->routeMatch->getParameter('commerce_order');

    $wallet_data = $order->getData('minter_wallet');
    if (empty($wallet_data)) {
      $form_state->setErrorByName('wallet', $this->t('Something went wrong. Please try again later.'));
    }

    // Get wallet balance and compare with the order balance.
    $wallet_balance = $this->minterApi->getBalance($wallet_data['address']);
    if (empty($wallet_balance)) {
      $form_state->setErrorByName('wallet', $this->t('Something went wrong. Please try again later.'));
    }

    $coin = $order->getBalance()->getCurrencyCode();

    // 1.00 is equal to 1000000000000000000 in the balance response.
    $wallet_balance = $wallet_balance->result->balance->{$coin} / 1000000000000000000;

    // We still expect some BIPs.
    if ($order->getBalance()->getNumber() > $wallet_balance) {
      // User didn't pay at all.
      if ($wallet_balance === 0) {
        $form_state->setErrorByName('wallet', $this->t("We didn't receive your payment yet. Please try again later."));
      }
      else {
        // User already paid something.
        $form_state->setErrorByName('wallet', $this->t("We received a partial payment (@received). Remaining payment: @number @currency.", [
          '@received' => $wallet_balance,
          '@number' => $order->getBalance()->getNumber() - $wallet_balance,
          '@currency' => $order->getBalance()->getCurrencyCode(),
        ]));
      }
    }
    // TODO: Should we do anything if user paid more?
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;
    $payment_method->set('wallet', $form['wallet']['#value']);

    /** @var \Drupal\commerce_minter\Plugin\Commerce\PaymentGateway\MinterGateway $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;
    $payment_gateway_plugin->createPaymentMethod($payment_method, []);
  }

}
