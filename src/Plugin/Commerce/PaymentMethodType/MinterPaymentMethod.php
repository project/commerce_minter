<?php

namespace Drupal\commerce_minter\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Provides the Minter payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "minter",
 *   label = @Translation("Minter"),
 * )
 */
class MinterPaymentMethod extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    $args = [
      '@wallet' => $payment_method->wallet->value,
    ];
    return $this->t('Wallet (@wallet)', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['wallet'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Customer Wallet'))
      ->setDescription(t('The customer wallet used for payment.'))
      ->setRequired(TRUE);

    return $fields;
  }

}
