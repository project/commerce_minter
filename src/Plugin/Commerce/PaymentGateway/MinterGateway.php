<?php

namespace Drupal\commerce_minter\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\profile\Entity\Profile;
use Minter\MinterAPI;
use Minter\SDK\MinterCoins\MinterSendCoinTx;
use Minter\SDK\MinterTx;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Minter payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "minter",
 *   label = "Minter",
 *   display_label = "Minter",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_minter\PluginForm\MinterPaymentForm",
 *   },
 *   payment_method_types = {"minter"},
 * )
 */
class MinterGateway extends OnsitePaymentGatewayBase {

  /** @var \Minter\MinterAPI */
  protected $minterApi;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, MinterAPI $minter_api) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->minterApi = $minter_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('minter.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'wallet' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['wallet'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wallet'),
      '#default_value' => $this->configuration['wallet'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['wallet'] = $values['wallet'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    // Drupal Commerce expects billing information in the payment data.
    // Here is a fake Billing Address just to make it happy.
    $profile = Profile::create(['type' => 'customer']);
    $payment_method->setBillingProfile($profile);

    $payment_method->setReusable(FALSE);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $payment->setState('completed');
    $payment->save();

    $this->transferAmount($payment);
  }

  /**
   * Sends received amount to the master wallet.
   */
  protected function transferAmount(PaymentInterface $payment) {
    $wallet = $payment->getOrder()->getData('minter_wallet');
    $amount = $payment->getAmount();

    // Wallet amount - 0.01 is the same as "Use Max" button.
    $tx = new MinterTx([
      'nonce' => $this->minterApi->getNonce($wallet['address']),
      'chainId' => MinterTx::MAINNET_CHAIN_ID,
      'gasPrice' => (int) $this->minterApi->getMinGasPrice()->result,
      'gasCoin' => $amount->getCurrencyCode(),
      'type' => MinterSendCoinTx::TYPE,
      'data' => [
        'coin' => $amount->getCurrencyCode(),
        'to' => $this->configuration['wallet'],
        // @TODO: Get fee from signed transaction.
        'value' => $amount->getNumber() - 0.01,
      ],
      'payload' => '',
      'serviceData' => '',
      'signatureType' => MinterTx::SIGNATURE_SINGLE_TYPE,
    ]);

    $sign = $tx->sign($wallet['private_key']);
    $result = $this->minterApi->send($sign);
    if (empty($result->result->hash)) {
      // TODO: Log something into database in that case.
    }
  }

}
