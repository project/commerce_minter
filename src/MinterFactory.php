<?php

namespace Drupal\commerce_minter;

use Minter\MinterAPI;

/**
 * Defines the Minter factory.
 */
class MinterFactory {

  /**
   * Create Minter client.
   *
   * @return \Minter\MinterAPI
   *   Minter PHP SDK client.
   */
  public function create() {
    // TODO: Make Node URL configurable.
    $nodeUrl = 'http://api-01.minter.store:8841';
    return new MinterApi($nodeUrl);
  }

}
